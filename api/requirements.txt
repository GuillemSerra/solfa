-i https://pypi.org/simple
aiodine==1.2.7
aiodns==2.0.0
aiohttp==3.5.4
aiopg==0.16.0
async-timeout==3.0.1
attrs==19.1.0
bocadillo==0.17.0
cchardet==2.1.4
certifi==2019.6.16
cffi==1.12.3
chardet==3.0.4
click==7.0
h11==0.8.1
httptools==0.0.13 ; sys_platform != 'win32' and sys_platform != 'cygwin' and platform_python_implementation != 'pypy'
idna==2.8
jinja2==2.10.1
lark-parser==0.6.4
markupsafe==1.1.1
multidict==4.5.2
psycopg2-binary==2.8.3
pycares==3.0.0
pycparser==2.19
python-multipart==0.0.5
pytz==2019.1
requests==2.22.0
six==1.12.0
starlette==0.12.2
tartiflette-starlette==0.2.0
tartiflette==0.10.2
typesystem==0.2.4
urllib3==1.25.3
uvicorn==0.8.3
uvloop==0.12.2 ; sys_platform != 'win32' and sys_platform != 'cygwin' and platform_python_implementation != 'pypy'
websockets==7.0
whitenoise==4.1.2
yarl==1.3.0
