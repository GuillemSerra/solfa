CREATE TABLE book (
   id serial PRIMARY KEY,
   title VARCHAR (255) NOT NULL,
   author VARCHAR (255) NOT NULL,
   price INTEGER
);


CREATE TABLE recipe (
   id serial PRIMARY KEY,
   name VARCHAR (255) UNIQUE NOT NULL,
   cooking_time INTEGER NOT NULL,
   book_id INTEGER REFERENCES book(id)
);


CREATE TABLE ingredient (
   id serial PRIMARY KEY,
   recipe_id INTEGER REFERENCES recipe(id),
   name VARCHAR (255) NOT NULL,
   quantity FLOAT NOT NULL,
   type VARCHAR (30) NOT NULL
);


INSERT INTO book VALUES (1, 'Titol del primer', 'Guillem Serra', 50);
INSERT INTO book VALUES (2, 'Titol del segon', 'Guillem Padró', 6230);
INSERT INTO book VALUES (3, 'Titol del tercer', 'Guillem Sorró', 923);


INSERT INTO recipe VALUES (1, 'Tartiflette by Eric Guelpa', 15, 1);
INSERT INTO recipe VALUES (2, 'La vraie Tartiflette', 20, 1);
INSERT INTO recipe VALUES (3, 'Tartiflette by Alain Ducasse', 25, 2);
INSERT INTO recipe VALUES (4, 'aydiomio by Someone', 30, 2);
INSERT INTO recipe VALUES (5, 'baia by Else', 5, 3);


INSERT INTO ingredient VALUES (1, 1, 'potato', 10, 'UNIT');
INSERT INTO ingredient VALUES (2, 1, 'onion', 2, 'UNIT');
INSERT INTO ingredient VALUES (3, 1, 'bacon', 100, 'GRAM');
INSERT INTO ingredient VALUES (4, 1, 'white wine', 0.05, 'LITER');
INSERT INTO ingredient VALUES (5, 1, 'reblochon AOP', 1, 'UNIT');


INSERT INTO ingredient VALUES (6, 2, 'potato', 1000, 'GRAM');
INSERT INTO ingredient VALUES (7, 2, 'bacon', 200, 'GRAM');
INSERT INTO ingredient VALUES (8, 2, 'onion', 200, 'GRAM');
INSERT INTO ingredient VALUES (9, 2, 'reblochon AOP', 1, 'UNIT');
INSERT INTO ingredient VALUES (10, 2, 'tablespoon of oil', 2, 'UNIT');
INSERT INTO ingredient VALUES (11, 2, 'clove of garlic', 1, 'UNIT');


INSERT INTO ingredient VALUES (12, 3, 'potato', 1000, 'GRAM');
INSERT INTO ingredient VALUES (13, 3, 'smoked bacon', 200, 'GRAM');
INSERT INTO ingredient VALUES (14, 3, 'onion', 2, 'UNIT');
INSERT INTO ingredient VALUES (15, 3, 'reblochon AOP', 1, 'UNIT');
INSERT INTO ingredient VALUES (16, 3, 'fresh cream', 0.1, 'LITER');
INSERT INTO ingredient VALUES (17, 3, 'butter', 40, 'GRAM');
INSERT INTO ingredient VALUES (18, 3, 'tablespoon of pepper', 1, 'UNIT');


INSERT INTO ingredient VALUES (19, 4, 'potato', 1000, 'GRAM');
INSERT INTO ingredient VALUES (20, 4, 'smoked bacon', 200, 'GRAM');
INSERT INTO ingredient VALUES (21, 4, 'onion', 2, 'UNIT');
INSERT INTO ingredient VALUES (22, 4, 'reblochon AOP', 1, 'UNIT');
INSERT INTO ingredient VALUES (23, 4, 'fresh cream', 0.1, 'LITER');
INSERT INTO ingredient VALUES (24, 4, 'butter', 40, 'GRAM');
INSERT INTO ingredient VALUES (25, 4, 'tablespoon of pepper', 1, 'UNIT');


INSERT INTO ingredient VALUES (26, 5, 'potato', 1000, 'GRAM');
INSERT INTO ingredient VALUES (27, 5, 'smoked bacon', 200, 'GRAM');
INSERT INTO ingredient VALUES (28, 5, 'onion', 2, 'UNIT');
INSERT INTO ingredient VALUES (29, 5, 'reblochon AOP', 1, 'UNIT');
INSERT INTO ingredient VALUES (30, 5, 'fresh cream', 0.1, 'LITER');
INSERT INTO ingredient VALUES (31, 5, 'butter', 40, 'GRAM');
INSERT INTO ingredient VALUES (32, 5, 'tablespoon of pepper', 1, 'UNIT');
