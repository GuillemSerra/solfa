import json

from bocadillo import Router

from api.books.factories import build_get_book_use_case


books_router = Router()


@books_router.route('/book/{book_id}')
async def get_book(req, res, book_id: int, db_conn):
    uc = build_get_book_use_case(db_conn)
    res.text = json.dumps(await uc.execute(book_id))
