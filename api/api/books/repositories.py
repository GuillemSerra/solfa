class BookRepository:

    def __init__(self, db_conn):
        self.db = db_conn

    async def get_book(self, book_id: int):
        await self.db.execute("SELECT * FROM book WHERE id=%s", (book_id, ))
        book = await self.db.fetchone()

        return {
            "id": book[0],
            "title": book[1],
            "author": book[2],
            "price": book[3]
        }
