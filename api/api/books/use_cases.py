class GetBookUseCase:

    def __init__(self, book_repo):
        self.book_repo = book_repo

    async def execute(self, book_id: int):
        return await self.book_repo.get_book(book_id)
