from api.books.use_cases import GetBookUseCase
from api.books.repositories import BookRepository


def build_get_book_use_case(db_conn) -> GetBookUseCase:
    return GetBookUseCase(
        BookRepository(db_conn)
    )
