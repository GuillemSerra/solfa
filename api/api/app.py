import os

from api.recipes.views import recipes_router
from api.books.views import books_router

from bocadillo import App, discover_providers
from tartiflette import Engine
from tartiflette_starlette import TartifletteApp


def build_app():
    engine = Engine(
        sdl=os.path.dirname(os.path.abspath(__file__)) + "/sdl",
        modules=[
            "api.recipes.resolvers.query_resolvers",
            "api.recipes.resolvers.mutation_resolvers",
        ],
    )

    graphql = TartifletteApp(
        engine=engine,
        graphiql=True
    )

    app = App()
    discover_providers("api.providers.db", "api.providers.http")
    app.mount("/graphql", graphql)
    app.include_router(recipes_router)
    app.include_router(books_router)

    return app


app = build_app()
