import logging

from starlette.config import Config

config = Config(".env")

logging.basicConfig(level=logging.DEBUG, format='%(name)s: %(asctime)s (%(funcName)s) %(message)s')

DEBUG = config("DEBUG", default=False, cast=bool)

DB_HOST = config("DB_HOST")
DB_USER = config("DB_USER")
DB_PSWD = config("DB_PSWD")
DB_NAME = config("DB_NAME")
