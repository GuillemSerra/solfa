import aiohttp

from bocadillo import provider


@provider(scope="app")
async def http_client():
    return aiohttp.ClientSession()
