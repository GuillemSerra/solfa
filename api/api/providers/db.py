import aiopg

from bocadillo import provider, settings


_POOL = None


async def start_pool():
    global _POOL
    dsn = f"dbname={settings.DB_NAME} user={settings.DB_USER} password={settings.DB_PSWD} host={settings.DB_HOST}"
    _POOL = await aiopg.create_pool(dsn, minsize=10, maxsize=50)


@provider
async def db_conn():

    if not _POOL:
        await start_pool()

    async with _POOL.acquire() as conn:
        async with conn.cursor() as cur:
            yield cur
