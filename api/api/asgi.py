from bocadillo import configure
from api.app import app
from . import settings


configure(app, settings)
