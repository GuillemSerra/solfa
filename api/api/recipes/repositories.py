import logging
import json

from bocadillo import settings


logger = logging.getLogger(__name__)


class RecipesRepository:

    def __init__(self, db_conn):
        self.db = db_conn

    async def get_all(self):
        await self.db.execute("SELECT * FROM recipe")
        results = await self.db.fetchall()

        return [{
            "id": id,
            "name": name,
            "cookingTime": cooking_time
        } for id, name, cooking_time, _ in results]

    async def get_recipe(self, recipe_id: int):
        await self.db.execute(
            "SELECT * FROM recipe WHERE id=%s",
            (recipe_id,)
        )
        result = await self.db.fetchone()

        return {
            "id": result[0],
            "name": result[1],
            "cookingTime": result[2]
        }


class IngredientsRepository:

    def __init__(self, db_conn):
        self.db = db_conn

    async def get_ingredients_from_recipe(self, recipe_id: int):
        await self.db.execute(
            "SELECT * FROM ingredient WHERE recipe_id=%s",
            (recipe_id, )
        )
        results = await self.db.fetchall()

        return [{
            "name": name,
            "quantity": quantity,
            "type": type
        } for _, _, name, quantity, type in results]


class BookRepository:

    BOOK_SERVICE_URL = 'http://api:8000/book/'

    def __init__(self, http_client):
        self.http = http_client

    async def get_book(self, book_id: int):
        async with self.http.get(self.BOOK_SERVICE_URL + str(book_id)) as resp:
            logger.info(resp.status)
            result = await resp.text()

        return json.loads(result)
