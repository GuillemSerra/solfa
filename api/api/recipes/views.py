import logging

from bocadillo import Router

from api.recipes.factories import build_get_recipe_use_case


recipes_router = Router()
logger = logging.getLogger(__name__)


@recipes_router.route("/recipe/{recipe_id}")
async def get_recipe(req, res, recipe_id: int, db_conn):
    uc = build_get_recipe_use_case(db_conn)
    res.text = await uc.execute(recipe_id)
