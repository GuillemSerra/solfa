from api.recipes.use_cases import (
    GetRecipeUseCase,
    GetIngredientsUseCase,
    ListRecipesUseCase,
    GetBookUseCase
)
from api.recipes.repositories import (
    RecipesRepository,
    IngredientsRepository,
    BookRepository
)


def build_get_recipe_use_case(db_conn) -> GetRecipeUseCase:
    return GetRecipeUseCase(
        RecipesRepository(db_conn)
    )


def build_get_ingredients_use_case(db_conn) -> GetIngredientsUseCase:
    return GetIngredientsUseCase(
        IngredientsRepository(db_conn)
    )


def build_list_recipes_use_case(db_conn) -> ListRecipesUseCase:
    return ListRecipesUseCase(
        RecipesRepository(db_conn)
    )


def build_get_book_use_case(http_client) -> GetBookUseCase:
    return GetBookUseCase(
        BookRepository(http_client)
    )
