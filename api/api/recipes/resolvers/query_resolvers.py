import asyncio
import logging

from tartiflette import Resolver
from bocadillo.injection import consumer

from api.recipes.factories import (
    build_get_recipe_use_case,
    build_get_ingredients_use_case,
    build_list_recipes_use_case,
    build_get_book_use_case
)


logger = logging.getLogger(__name__)


@Resolver("Query.recipes")
async def resolve_recipes(parent, args, ctx, info):
    await asyncio.sleep(1)

    @consumer
    async def get(db_conn):
        uc = build_list_recipes_use_case(db_conn)
        return await uc.execute()

    return await get()


@Resolver("Query.recipe")
async def resolve_recipe(parent, args, ctx, info):
    await asyncio.sleep(1)

    @consumer
    async def get(db_conn, *params, **kwargs):
        uc = build_get_recipe_use_case(db_conn)
        return await uc.execute(*params)

    return await get(int(args["id"]))


@Resolver("Recipe.ingredients")
async def resolve_ingredients(parent, args, ctx, info):
    await asyncio.sleep(1)

    @consumer
    async def get(db_conn, *params, **kwargs):
        uc = build_get_ingredients_use_case(db_conn)
        return await uc.execute(*params)

    return await get(int(parent["id"]))


@Resolver("Query.book")
async def resolve_book(parent, args, ctx, info):

    @consumer
    async def get(http_client, *params, **kwargs):
        uc = build_get_book_use_case(http_client)
        return await uc.execute(*params)

    return await get(int(args["id"]))
