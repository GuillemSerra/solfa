class ListRecipesUseCase:

    def __init__(self, recipes_repo):
        self.recipes_repo = recipes_repo

    async def execute(self):
        return await self.recipes_repo.get_all()


class GetRecipeUseCase:

    def __init__(self, recipes_repo):
        self.recipes_repo = recipes_repo

    async def execute(self, recipe_id: int):
        return await self.recipes_repo.get_recipe(recipe_id)


class GetIngredientsUseCase:

    def __init__(self, ingredients_repo):
        self.ingredients_repo = ingredients_repo

    async def execute(self, recipe_id: int):
        return await self.ingredients_repo.get_ingredients_from_recipe(recipe_id)


class GetBookUseCase():

    def __init__(self, books_repo):
        self.books_repo = books_repo

    async def execute(self, book_id: int):
        return await self.books_repo.get_book(book_id)
